<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\ElasticSearch;

use Fittinq\Symfony\Connector\ElasticSearch\ServiceContext;
use PHPUnit\Framework\TestCase;

class ServiceContextTest extends TestCase
{
    private ServiceContext $contextResolver;

    protected function setUp(): void
    {
        parent::setUp();

        $this->contextResolver = new ServiceContext('my_service');
    }

    public function test_addCustomContextToServiceContextResolver_returnServiceInformationAndMergeCustomContext()
    {
        $expected = [
            'pid' => ServiceContext::$PID,
            'service' => 'my_service',
            'fruit' => 'banana',
            'color' => 'blue'
        ];

        $this->assertEquals($expected, $this->contextResolver->getContext(['color' => 'blue', 'fruit' => 'banana']));
    }

    public function test_addCustomContextAndOverwriteValueToServiceContextResolver_returnServiceInformationAndOverwriteCustomContext()
    {
        $expected = [
            'pid' => 'my_new_pid',
            'service' => 'my_service',
        ];

        $this->assertEquals($expected, $this->contextResolver->getContext(['pid' => 'my_new_pid']));
    }
}
