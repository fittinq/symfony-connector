<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class NotLoggingAwareController extends AbstractController implements IndexAwareInterface
{
    public function index()
    {
    }
}