<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Controller;

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class KernelMock implements KernelInterface
{

    public function getBuildDir(): string
    {
        // TODO: Implement handle() method.
    }

    public function handle(Request $request, int $type = self::MAIN_REQUEST, bool $catch = true): Response
    {
        // TODO: Implement handle() method.
    }

    public function registerBundles(): array
    {
        // TODO: Implement registerBundles() method.
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        // TODO: Implement registerContainerConfiguration() method.
    }

    public function boot()
    {
        // TODO: Implement boot() method.
    }

    public function shutdown()
    {
        // TODO: Implement shutdown() method.
    }

    public function getBundles(): array
    {
        // TODO: Implement getBundles() method.
    }

    public function getBundle(string $name): BundleInterface
    {
        // TODO: Implement getBundle() method.
    }

    public function locateResource(string $name): string
    {
        // TODO: Implement locateResource() method.
    }

    public function getEnvironment(): string
    {
        // TODO: Implement getEnvironment() method.
    }

    public function isDebug(): bool
    {
        // TODO: Implement isDebug() method.
    }

    public function getProjectDir(): string
    {
        // TODO: Implement getProjectDir() method.
    }

    public function getContainer(): ContainerInterface
    {
        // TODO: Implement getContainer() method.
    }

    public function getStartTime(): float
    {
        // TODO: Implement getStartTime() method.
    }

    public function getCacheDir(): string
    {
        // TODO: Implement getCacheDir() method.
    }

    public function getLogDir(): string
    {
        // TODO: Implement getLogDir() method.
    }

    public function getCharset(): string
    {
        // TODO: Implement getCharset() method.
    }
}