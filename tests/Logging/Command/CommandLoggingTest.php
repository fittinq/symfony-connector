<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Command;

use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Test\Fittinq\Symfony\Mock\Logger\LoggerMock;
use Throwable;
use UnexpectedValueException;

class CommandLoggingTest extends TestCase
{
    private Configuration $configuration;
    private LoggerMock $loggerMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->loggerMock = $this->configuration->getLogger();
    }

    public function test_theCommandIsNotALoggingAwareCommand_doNotLogCommandCall()
    {
        $eventSubscriber = $this->configuration->create([], [], new NotLoggingAwareCommand());
        $beforeCommandEvent = $this->configuration->getBeforeCommandEvent();

        $eventSubscriber->onCommand($beforeCommandEvent);

        $this->loggerMock->expectNotToLogAnything();
    }

    public function test_theCommandIsNotALoggingAwareCommand_doNotLogOutput()
    {
        $exceptionMessage = 'test_logInvalidCommand_logAsError';
        $eventSubscriber = $this->configuration->create(
            ['command' => 'sandbox:play'],
            [],
            new NotLoggingAwareCommand(),
            Command::INVALID,
            new UnexpectedValueException($exceptionMessage)
        );

        $eventSubscriber->onError($this->configuration->getAfterCommandErrorEvent());
        $eventSubscriber->onTerminate($this->configuration->getAfterCommandTerminationEvent());

        $this->loggerMock->expectNotToLogAnything();
    }

    public function test_theCommandIsALoggingAwareCommand_logInput()
    {
        $eventSubscriber = $this->configuration->create(
            ['command' => 'sandbox:play', 'color' => '#ff00bb'],
            ['shape' => 'cube', 'flower' => 'rose'],
            new LoggingAwareCommand()
        );
        $beforeCommandEvent = $this->configuration->getBeforeCommandEvent();

        $eventSubscriber->onCommand($beforeCommandEvent);

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            $ sandbox:play
            
            color: #ff00bb
            
            shape: cube
            flower: rose
            EOT
        );
    }

    public function test_loggingCallFailsWithException_catchException()
    {
        $eventSubscriber = $this->configuration->create(
            ['command' => 'sandbox:play', 'color' => '#ff00bb'],
            ['shape' => 'cube', 'flower' => 'rose'],
            new LoggingAwareCommand()
        );
        $beforeCommandEvent = $this->configuration->getBeforeCommandEvent();
        $this->loggerMock->setUpWithException($this->createMock(Throwable::class));

        $eventSubscriber->onCommand($beforeCommandEvent);

        $this->assertTrue(true);
    }

    public function test_loggingOutputFailsWithException_catchException()
    {
        $exceptionMessage = 'test_logInvalidCommand_logAsError';
        $eventSubscriber = $this->configuration->create(
            ['command' => 'sandbox:play'],
            [],
            new LoggingAwareCommand(),
            Command::INVALID,
            new UnexpectedValueException($exceptionMessage)
        );

        $this->loggerMock->setUpWithException($this->createMock(Throwable::class));

        $eventSubscriber->onError($this->configuration->getAfterCommandErrorEvent());
        $eventSubscriber->onTerminate($this->configuration->getAfterCommandTerminationEvent());

        $this->assertTrue(true);
    }

    public function test_logSuccessfulCommand_logSuccessMessage()
    {
        $eventSubscriber = $this->configuration->create(
            ['command' => 'sandbox:play'],
            [],
            new LoggingAwareCommand(),
            Command::SUCCESS
        );

        $eventSubscriber->onTerminate($this->configuration->getAfterCommandTerminationEvent());

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::INFO,
            <<<EOT
            $ sandbox:play
            
            exit code: 0
            EOT
        );
    }

    public function test_logInvalidCommand_logAsError()
    {
        $exceptionMessage = 'test_logInvalidCommand_logAsError';
        $eventSubscriber = $this->configuration->create(
            ['command' => 'sandbox:play'],
            [],
            new LoggingAwareCommand(),
            Command::INVALID,
            new UnexpectedValueException($exceptionMessage)
        );

        $eventSubscriber->onError($this->configuration->getAfterCommandErrorEvent());
        $eventSubscriber->onTerminate($this->configuration->getAfterCommandTerminationEvent());

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::ERROR,
            <<<EOT
            $ sandbox:play
            
            exit code: 2
            
            $exceptionMessage
            EOT
        );
    }

    public function test_logCommandFailure_logAsCritical()
    {
        $exceptionMessage = 'test_logCommandFailure_logAsCritical';
        $eventSubscriber = $this->configuration->create(
            ['command' => 'sandbox:play'],
            [],
            new LoggingAwareCommand(),
            Command::FAILURE,
            new UnexpectedValueException($exceptionMessage)
        );

        $eventSubscriber->onError($this->configuration->getAfterCommandErrorEvent());
        $eventSubscriber->onTerminate($this->configuration->getAfterCommandTerminationEvent());

        $this->loggerMock->expectToHaveLoggedLast(LogLevel::ERROR,
            <<<EOT
            $ sandbox:play
            
            exit code: 1
            
            $exceptionMessage
            EOT
        );
    }
}
