<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector\HttpClient;

use Fittinq\Symfony\Connector\Logging\HttpClientLogger;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpClient\HttpClientTrait;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\ResponseStreamInterface;
use Throwable;

class LogHttpClient implements HttpClientInterface
{
    use HttpClientTrait;

    private HttpClientLogger $logger;
    private HttpClientInterface $httpClient;

    public function __construct(HttpClientInterface $httpClient, HttpClientLogger $logger)
    {
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    /**
     * @throws Throwable
     */
    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        $parsedUrl = isset($options['query']) ? implode('', self::parseUrl($url, $options['query'])) : $url;

        try {
            $this->logger->logRequest($method, $parsedUrl, $options);
            $response = $this->makeRequest($method, $url, $options);
            $response->getStatusCode();
            $this->logger->logServiceAvailableResponse($method, $parsedUrl, $response);
            return $response;
        } catch (TransportException $e) {
            $this->logger->logServiceUnavailableResponse($method, $parsedUrl, $e);
            throw $e;
        }
    }

    public function stream($responses, float $timeout = null): ResponseStreamInterface
    {
        return $this->httpClient->stream($responses, $timeout);
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function makeRequest(string $method, string $url, array $options): ResponseInterface
    {
        return $this->httpClient->request($method, $url, $options);
    }

    public function withOptions(array $options): static
    {
        $clone = clone $this;
        $clone->httpClient = $this->httpClient->withOptions($options);

        return $clone;
    }
}
