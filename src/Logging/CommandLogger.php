<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector\Logging;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Throwable;

class CommandLogger
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function logCall(InputInterface $input): void
    {
        $arguments = $input->getArguments();
        $options = $input->getOptions();

        $message = '$ ' . array_shift($arguments) . "\n\n";
        $message .= $this->createKeyValueBlock($arguments);
        $message .= $this->createKeyValueBlock($options);

        $this->logger->info(trim($message));
    }

    private function createKeyValueBlock(array $arguments): string
    {
        $message = "";

        foreach ($arguments as $key => $value) {
            $message .= "{$key}: {$value}\n";
        }

        return $message . "\n";
    }

    public function logTermination(InputInterface $input, int $exitCode, ?Throwable $exception)
    {
        $arguments = $input->getArguments();
        $message = '$ ' . array_shift($arguments) . "\n\n";
        $message .= "exit code: {$exitCode}\n\n";

        if ($exception) {
            $message .= $exception->getMessage();
        }

        if ($exitCode === Command::SUCCESS) {
            $this->logger->info(trim($message));
        } else {
            $this->logger->error($message);
        }
    }
}
