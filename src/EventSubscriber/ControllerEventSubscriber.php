<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector\EventSubscriber;

use Fittinq\Symfony\Connector\Logging\ControllerLogger;
use Fittinq\Symfony\Connector\Logging\LoggingAwareInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Throwable;

class ControllerEventSubscriber implements EventSubscriberInterface
{
    /**
     * This static var is used to check whether the response must be logged. We only log responses if the
     * request itself was also logged. We resolve this by request method and request URI.
     *
     * The reason for this hack is the fact that the onKernelResponse method does not directly have access to the
     * controller being called.
     */
    public static array $LOGGED_REQUEST_SIGNATURES = [];

    private ControllerLogger $logger;

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }

    public function __construct(ControllerLogger $controllerLogger)
    {
        $this->logger = $controllerLogger;
    }

    public function onKernelController(ControllerEvent $event): void
    {
        try {
            if (!$this->shouldLogRequest($event)) {
                return;
            }
            $this->logger->logRequest($event->getRequest());

        } catch (Throwable) {
            // if logs fail, we should not let the service itself fail.
        }
    }

    private function shouldLogRequest(ControllerEvent $event): bool
    {
        $controller = $event->getController()[0];
        $shouldLogRequest = $controller instanceof LoggingAwareInterface;

        /** This if statement is a deliberate side effect @see $LOGGED_REQUEST_SIGNATURES */
        if ($shouldLogRequest) {
            $request = $event->getRequest();
            static::$LOGGED_REQUEST_SIGNATURES[] = "{$request->getMethod()} {$request->getUri()}";
        }

        return $shouldLogRequest;
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        try {
            $request = $event->getRequest();

            if (!$this->shouldLogResponse($request)) {
                return;
            }

            $this->logger->logResponse($request, $event->getResponse());
        } catch (Throwable) {
            // if logs fail, we should not let the service itself fail.
        }
    }

    private function shouldLogResponse(Request $request): bool
    {
        return in_array("{$request->getMethod()} {$request->getUri()}", static::$LOGGED_REQUEST_SIGNATURES);
    }
}
